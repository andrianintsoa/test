import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from './http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class TypeDocumentService {

  constructor(private httpClientService: HttpClientService) { }

  typedocuments(): Observable<any> {
    return this.httpClientService.get(`type-documents`);
  }

  addTypeDocument(data: any): Observable<any> {
    return this.httpClientService.post(`type-documents`, data);
  }

  detailTypeDocument(id: string): Observable<any> {
    return this.httpClientService.get(`type-documents/${id}`);
  }

  editTypeDocument(data: any): Observable<any> {
    return this.httpClientService.put(`type-documents/${data.id}`, data);
  }

  deleteTypeDocument(id: string): Observable<any> {
    return this.httpClientService.delete(`type-documents/${id}`);
  }
}
