import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { HttpClientService } from './http-client/http-client.service';
import { StorageService } from './storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class DemandecinService {

  uri_api = `http://localhost:8080/api/`;

  constructor(
    private httpClientService: HttpClientService,
    private http: HttpClient,
    private storageService: StorageService
  ) { }

  demandecins(params: { [key: string]: string | number } = {}): Observable<any> {
    return this.httpClientService.get('demande-cins', params);
  }

  demandecinsUtilisateur(params: { [key: string]: string | number } = {}): Observable<any> {
    return this.httpClientService.get('demande-cins/user', params);
  }

  createDemandecinWithDocuments(payload): Observable<any> {
    const formData = new FormData();
    formData.append('demande', JSON.stringify(payload.demande));
    formData.append('photo', payload.photo);

    for (let i = 0; i < payload.docs.length; i++) {
      formData.append('docs', payload.docs[i], payload.docs[i].name);
    }

    return this.httpClientService.postWithFile(`demande-cins`, formData);

  }

  updateStatutDemandecin(id: number, statut: string, motif = null): Observable<any> {
    const requestBody: any = { statut };
    if (motif) requestBody.motifRefus = motif
    return this.httpClientService.post(`demande-cins/statut/${id}`, requestBody);
  }

  editDemandecin(data: any): Observable<any> {
    return this.httpClientService.put(`demande-cins/${data.id}`, data);
  }

  detailDemandeCin(id: string): Observable<any> {
    return this.httpClientService.get(`demande-cins/${id}`)
  }

  delete(id: string): Observable<any> {
    return this.httpClientService.delete(`demande-cins/${id}`)
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('Une erreur s\'est produite :', error.error.message);
    } else {
      console.error(`Erreur du serveur, code : ${error.status}, corps : ${error.error}`);
    }
    return throwError('Une erreur s\'est produite. Veuillez réessayer plus tard.');
  }
}
