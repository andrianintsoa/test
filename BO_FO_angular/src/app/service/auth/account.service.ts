import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { StorageService } from '../storage/storage.service';
import { HttpClientService } from '../http-client/http-client.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(
    public router: Router,
    private storageService: StorageService,
    private httpClientService: HttpClientService
  ) {

  }

  login(data) {
    return this.httpClientService.post(`auth/signin`, data).pipe(
      map((res: any) => {
        if (res?.token) this.storageService.setToken(res?.token);
        if (res?.user) {
          this.storageService.setUserInfo(res?.user);
          return true
        }
        return false
      })
    )
  }

  async logout() {

    localStorage.clear();
    this.router.navigate(['/']);

  }

  signUp(data) {
    return this.httpClientService.post(`auth/signup`, data);
  }

  recoverPassword(email: string) {
    // return this.afAuth.auth.sendPasswordResetEmail(email, { url: Key.HOST + '/auth/login' });
  }

  get isLoggedIn(): boolean {
    const user = this.storageService.getUserInfo();
    return user !== null;
  }

  checkUserState() {
    if (!this.isLoggedIn) {
      this.router.navigate(['auth/login']);
    }
  }

  hasRole(name: string): boolean {
    const user = this.storageService.getUserInfo();
    return user?.role?.nom === name;
  }

}
