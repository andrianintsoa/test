import { Injectable, EventEmitter } from "@angular/core";
import { TOKEN, USER_INFO } from "../../constants/storage.constants";

@Injectable({
  providedIn: 'root'
})

export class StorageService {

  myAccountEventEmitter: EventEmitter<any> = new EventEmitter();

  constructor() { }

  emitEvent(data: any) {
    this.myAccountEventEmitter.emit(data);
  }

  getToken() {
    return JSON.parse(localStorage.getItem(TOKEN) ?? null);
  }

  setToken(token: string) {
    localStorage.setItem(TOKEN, JSON.stringify(token));
  }

  getUserInfo() {
    const info = localStorage.getItem(USER_INFO)
    let result = null
    if (info) {
      result = JSON.parse(info)
    }
    return result
  }

  setUserInfo(userInfo: string) {
    localStorage.setItem(USER_INFO, JSON.stringify(userInfo));
  }

  removeToken() {
    localStorage.removeItem(TOKEN);
  }

  removeUserInfo() {
    localStorage.removeItem(USER_INFO);
  }

}
