import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { HttpClientService } from './http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class CinService {


  constructor(private httpClientService: HttpClientService) { }

  cins(): Observable<any> {
    return this.httpClientService.get('cins');
  }

  detailCin(id: string): Observable<any> {
    return this.httpClientService.get(`cins/${id}`)
  }

  delete(id: string): Observable<any> {
    return this.httpClientService.delete(`cins/${id}`)
  }

  testeScan(id: string, payload): Observable<any> {
    return this.httpClientService.post(`cins/scanQr/${id}`, payload)
  }

  cinsByUser(id): Observable<any> {
    return this.httpClientService.get('cins/by-user/' + id);
  }

  historiqueScan(payload): Observable<any> {
    return this.httpClientService.get('historiques-paginate', payload, true);
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('Une erreur s\'est produite :', error.error.message);
    } else {
      console.error(`Erreur du serveur, code : ${error.status}, corps : ${error.error}`);
    }
    return throwError('Une erreur s\'est produite. Veuillez réessayer plus tard.');
  }
}
