import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { StorageService } from '../storage/storage.service';
import { environment } from '../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { AccountService } from '../auth/account.service';
import { throwError } from 'rxjs';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
    private toastr: NbToastrService,
    private router: Router
  ) {

  }

  getHeaders(): HttpHeaders {
    const token = this.storageService.getToken()

    let headers = new HttpHeaders({
      // 'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });

    return headers;
  }


  get(url: string, params: { [key: string]: string | number } = {}, isMongo = false) {
    const baseUrl = isMongo ? environment.apiMongoBaseUrl : environment.apiBaseUrl;
    const queryParams = new HttpParams({ fromObject: params });
    return this.http.get(`${baseUrl}/api/${url}`, { headers: this.getHeaders(), params: queryParams }).pipe(
      catchError((error => this.handleError(error))),
      map(resp => {

        if (isMongo) {
          return resp
        }

        return this.handleSuccess(resp)
      })
    );
  }

  post(url: string, body: any) {
    return this.http.post(`${environment.apiBaseUrl}/api/${url}`, body, { headers: this.getHeaders() }).pipe(
      catchError((error => this.handleError(error))),
      map(resp => this.handleSuccess(resp))
    );
  }

  postWithFile(url: string, body: any) {

    const headers = this.getHeaders()

    return this.http.post(`${environment.apiBaseUrl}/api/${url}`, body, { headers }).pipe(
      catchError((error => this.handleError(error))),
      map(resp => this.handleSuccess(resp))
    );
  }

  put(url: string, body: any) {
    return this.http.put(`${environment.apiBaseUrl}/api/${url}`, body, { headers: this.getHeaders() }).pipe(
      catchError((error => this.handleError(error))),
      map(resp => this.handleSuccess(resp))
    );
  }

  delete(url: string) {
    return this.http.delete(`${environment.apiBaseUrl}/api/${url}`, { headers: this.getHeaders() }).pipe(
      catchError((error => this.handleError(error))),
      map(resp => this.handleSuccess(resp))
    );
  }

  handleError(error: any) {

    console.log('error', error)
    if (error.error?.error === 'Unauthorized') {
      localStorage.clear();
      this.router.navigate(['auth/login']);
    }
    if (error.error?.message) {
      this.toastr.show(error.error.message, 'Erreur', { status: 'danger', duration: 5000 });
    }
    return throwError(error);
  }

  handleSuccess(resp: any) {

    if (resp.message) {
      this.toastr.show(resp.message, 'Notification', { status: 'success' });
    }
    return resp.data;
  }

}
