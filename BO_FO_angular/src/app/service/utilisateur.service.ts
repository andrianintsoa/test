import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from './http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  constructor(private httpClientService: HttpClientService) { }

  utilisateurs(): Observable<any> {
    return this.httpClientService.get(`utilisateurs`);
  }

  addUtilisateur(data: any): Observable<any> {
    const formData = new FormData();
    formData.append('newPassword', data.newPassword);
    formData.append('utilisateur', JSON.stringify(data.utilisateur));
    return this.httpClientService.post(`utilisateurs`, formData);
  }

  detailUtilisateur(id: string): Observable<any> {
    return this.httpClientService.get(`utilisateurs/${id}`);
  }

  editUtilisateur(data: any): Observable<any> {
    const formData = new FormData();
    formData.append('newPassword', data.newPassword);
    formData.append('utilisateur', JSON.stringify(data.utilisateur));
    return this.httpClientService.put(`utilisateurs/${data.utilisateur.id}`, formData);
  }

  deleteUtilisateur(id: string): Observable<any> {
    return this.httpClientService.delete(`utilisateurs/${id}`);
  }

  roles(): Observable<any[]> {
    return this.httpClientService.get(`roles`);
  }

}
