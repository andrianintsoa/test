import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from './http-client/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class CommuneService {

  constructor(private httpClientService: HttpClientService) { }

  communes(): Observable<any> {
    return this.httpClientService.get(`communes`);
  }

  addCommune(data: any): Observable<any> {
    return this.httpClientService.post(`communes`, data);
  }

  detailCommune(id: string): Observable<any> {
    return this.httpClientService.get(`communes/${id}`);
  }

  editCommune(data: any): Observable<any> {
    return this.httpClientService.put(`communes/${data.id}`, data);
  }

  deleteCommune(id: string): Observable<any> {
    return this.httpClientService.delete(`communes/${id}`);
  }
}
