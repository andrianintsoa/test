import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule, NbSpinnerModule, NbSelectModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PdfViewerModule } from 'ng2-pdf-viewer';


import { UtilisateurRoutingModule } from './utilisateur-routing.module';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { UtilisateurComponent } from './utilisateur.component';

import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CreateUpdateComponent,
    UtilisateurComponent,
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    UtilisateurRoutingModule,
    NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule, NbSpinnerModule, NbSelectModule,
    Ng2SmartTableModule, PdfViewerModule,
    SharedModule
  ]
})
export class UtilisateurModule { }
