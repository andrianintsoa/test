import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateUpdateComponent } from './create-update/create-update.component';
import { UtilisateurComponent } from './utilisateur.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: UtilisateurComponent,
    },
    {
      path: 'edit',
      component: CreateUpdateComponent,
    },
    {
      path: 'edit/:id',
      component: CreateUpdateComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UtilisateurRoutingModule { }
