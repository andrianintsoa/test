import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UtilisateurService } from '../../../service/utilisateur.service';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { ROLE } from '../../../constants/constant';
import { StorageService } from '../../../service/storage/storage.service';


@Component({
  selector: 'ngx-cin-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent implements OnInit {

  formGroup: FormGroup;

  dataToEdit: any = {};
  isLoading = false
  title = ''
  roleList: any[] = []
  roleIdModel!: number
  isAdmin!: boolean

  constructor(
    private fb: FormBuilder,
    private service: UtilisateurService,
    private storageService: StorageService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this.title = id ? 'Modification' : 'Création'

    this.formGroup = this.fb.group({
      nom: ['', Validators.required],
      code: ['', Validators.pattern('[0-9]*')],
    });

    if (id) this.initForm(id)

    else {
      this.isLoading = true
      this.service.roles().subscribe(respRole => {
        this.roleList = respRole.filter(elt => elt.nom !== ROLE.ROLE_ADMIN.value).map(elt => ({ ...elt, label: ROLE[elt.nom].label }))
        this.isLoading = false
      })
    }
  }


  initForm(id): void {
    this.isLoading = true
    forkJoin(this.service.detailUtilisateur(id), this.service.roles())
      .subscribe(([respUser, respRole]) => {
        this.dataToEdit = respUser
        if (this.dataToEdit.role?.id) {
          this.roleIdModel = this.dataToEdit.role?.id
          this.isAdmin = this.dataToEdit.role.nom === ROLE.ROLE_ADMIN.value
        }

        this.roleList = respRole.filter(elt => elt.nom !== ROLE.ROLE_ADMIN.value).map(elt => ({ ...elt, label: ROLE[elt.nom].label }))
        this.isLoading = false
      }, error => {
        this.isLoading = false
        console.log('error', error)
      });

  }


  submit() {

    this.isLoading = true
    const role = this.roleList.find(elt => elt.label === this.roleIdModel)
    const payload: any = {
      utilisateur: { ...this.dataToEdit }
    }
    if (!this.isAdmin) {
      payload.utilisateur.role = role
    }

    delete payload.utilisateur.confirmPassword

    payload.newPassword = this.dataToEdit.password ? this.dataToEdit.password : null

    let subscription = this.dataToEdit?.id ? this.service.editUtilisateur(payload) : this.service.addUtilisateur(payload);

    subscription.subscribe(
      data => {
        console.log('data', data)
        if (this.isAdmin) {
          const user = this.storageService.setUserInfo(data)
          this.storageService.emitEvent(true)
        }
        this.back()
        this.isLoading = false
      }, error => {
        this.isLoading = false
        console.log('error', error)
      }
    )

  }

  back(): void {
    window.history.back();
  }

}
