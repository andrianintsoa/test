import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { Router } from '@angular/router';
import { GeneralTabSettings } from '../../shared/config/generalTabSettings';
import { UtilisateurService } from '../../service/utilisateur.service';
import { ROLE } from '../../constants/constant';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-Utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.scss']
})
export class UtilisateurComponent {

  settings = {
    ...GeneralTabSettings.appearance,
    columns: {
      nom: {
        title: 'Nom',
        type: 'string',
      },
      prenom: {
        title: 'Prenom',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
      telephone: {
        title: 'Téléphone',
        type: 'string',
      },
      role: {
        title: 'Rôle',
        type: 'string',
      },

    },
  };

  source: LocalDataSource = new LocalDataSource();

  isLoading = false

  constructor(
    private service: UtilisateurService,
    private router: Router,
    private toastr: NbToastrService,

  ) {
    this.fetchData()
  }

  fetchData(): any {
    this.isLoading = true
    this.service.utilisateurs().subscribe(data => {
      console.log('data', data)
      const dataSource = data.map(elt => {
        return {
          id: elt.id,
          nom: elt.nom ?? '-',
          prenom: elt.prenom ?? '-',
          email: elt.email ?? '-',
          telephone: elt.telephone ?? '-',
          role: ROLE[elt.role?.nom] ? ROLE[elt.role?.nom].label : '-',
          roleName: elt.role?.nom
        }
      })
      this.source.load(dataSource);
      this.isLoading = false
    }, error => {
      this.isLoading = false
      console.log('error', error)
    })
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Êtes-vous sûrs de réaliser cette action?')) {
      if (event.data.roleName === ROLE.ROLE_ADMIN.value) {
        this.toastr.danger('Vous ne pouvez pas supprimer l\'administrateur')
        return
      }
      this.isLoading = true
      this.service.deleteUtilisateur(event.data.id).subscribe(data => {
        this.fetchData()
      })
    } else {
      event.confirm.reject();
    }
  }

  edit(row) {
    console.log('row', row)
    this.router.navigate(['/pages/utilisateur/edit', { id: row.data.id }])
  }

  create(row) {
    console.log('row', row)
    this.router.navigate(['pages/utilisateur/edit'])
  }


}
