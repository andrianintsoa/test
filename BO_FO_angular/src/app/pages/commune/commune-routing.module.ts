import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateUpdateComponent } from './create-update/create-update.component';
import { CommuneComponent } from './commune.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: CommuneComponent,
    },
    {
      path: 'edit',
      component: CreateUpdateComponent,
    },
    {
      path: 'edit/:id',
      component: CreateUpdateComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommuneRoutingModule { }
