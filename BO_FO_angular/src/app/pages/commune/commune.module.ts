import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule, NbSpinnerModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PdfViewerModule } from 'ng2-pdf-viewer';


import { CommuneRoutingModule } from './commune-routing.module';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { CommuneComponent } from './commune.component';

import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CreateUpdateComponent,
    CommuneComponent,
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    CommuneRoutingModule,
    NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule, NbSpinnerModule,
    Ng2SmartTableModule, PdfViewerModule,
    SharedModule
  ]
})
export class CommuneModule { }
