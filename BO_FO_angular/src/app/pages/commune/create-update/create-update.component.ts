import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CommuneService } from '../../../service/commune.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-cin-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent implements OnInit {

  formGroup: FormGroup;

  dataToEdit?: any;
  isLoading = false
  title = ''

  constructor(
    private fb: FormBuilder,
    private service: CommuneService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this.title = id ? 'Modification' : 'Création'

    this.formGroup = this.fb.group({
      nom: ['', Validators.required],
      code: ['', Validators.pattern('[0-9]*')],
    });

    if (id) this.initForm(id)
  }


  initForm(id): void {
    this.isLoading = true
    this.service.detailCommune(id).subscribe(data => {
      this.dataToEdit = data
      this.formGroup.patchValue({
        nom: this.dataToEdit.nom,
        code: this.dataToEdit.code
      });
      this.isLoading = false
    }, error => {
      this.isLoading = false
      console.log('error', error)
    });

  }


  submit() {
    this.isLoading = true;
    const payload = {
      nom: this.formGroup.get('nom')?.value,
      code: this.formGroup.get('code')?.value,
    }
    let subscription = this.dataToEdit?.id ? this.service.editCommune({ id: this.dataToEdit.id, ...payload }) : this.service.addCommune(payload);

    subscription.subscribe(
      data => {
        console.log('data', data)
        this.isLoading = false;
        this.back()
      }, error => {
        this.isLoading = false;
        console.log('error', error)
      }
    )

  }

  back(): void {
    window.history.back();
  }

}
