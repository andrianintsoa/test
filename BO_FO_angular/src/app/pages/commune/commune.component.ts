import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { Router } from '@angular/router';
import { GeneralTabSettings } from '../../shared/config/generalTabSettings';
import { CommuneService } from '../../service/commune.service';

@Component({
  selector: 'ngx-Commune',
  templateUrl: './commune.component.html',
  styleUrls: ['./commune.component.scss']
})
export class CommuneComponent {

  settings = {
    ...GeneralTabSettings.appearance,
    columns: {
      id: {
        title: 'n°',
        type: 'string',

      },
      nom: {
        title: 'Nom',
        type: 'string',
      },
      code: {
        title: 'Code',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  isLoading = false

  constructor(
    private service: CommuneService,
    private router: Router,
  ) {
    this.fetchData()
  }

  fetchData(): any {
    this.isLoading = true
    this.service.communes().subscribe(data => {
      console.log('data', data)
      const dataSource = data
      this.source.load(dataSource);
      this.isLoading = false
    }, error => {
      this.isLoading = false
      console.log('error', error)
    })
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Êtes-vous sûrs de réaliser cette action?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  edit(row) {
    console.log('row', row)
    this.router.navigate(['/pages/commune/edit', { id: row.data.id }])
  }

  create(row) {
    console.log('row', row)
    this.router.navigate(['pages/commune/edit'])
  }


}
