import { Component, OnInit } from '@angular/core';

import { MENU_ADMIN_ITEMS, MENU_PUBLIC_ITEMS, MENU_EMPLOYEE_ITEMS } from './pages-menu';
import { StorageService } from '../service/storage/storage.service';
import { ROLE } from '../constants/constant';
import { CinService } from '../service/cin.service';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {

  menu!: any[]

  constructor(
    private storageService: StorageService,
    private cinService: CinService,
  ) {

  }

  ngOnInit(): void {
    const user = this.storageService.getUserInfo()
    const role = user.role
    switch (role.nom) {
      case ROLE.ROLE_ADMIN.value:
        this.menu = MENU_ADMIN_ITEMS
        break
      case ROLE.ROLE_EMPLOYEE.value:
        this.menu = MENU_EMPLOYEE_ITEMS
        break
      case ROLE.ROLE_USER.value:
        this.menu = MENU_PUBLIC_ITEMS
        break
      default:
        this.menu = []
        break
    }
    // this.cinService.testeScan('4', { idUtilisateur: user.id }).subscribe(resp => {
    //   console.log('###########', resp)
    // }, error => {
    //   console.log('####### error', error)
    // })
  }


}

