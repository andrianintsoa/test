import { NgModule } from '@angular/core';
import { NbCardModule, NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { HomePageComponent } from './home-page/home-page.component';
@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbCardModule
  ],
  declarations: [
    PagesComponent,
    UnauthorizedComponent,
    HomePageComponent
  ],
})
export class PagesModule {
}
