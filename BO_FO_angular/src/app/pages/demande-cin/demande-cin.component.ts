import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { DatePipe } from '@angular/common';

import { Router } from '@angular/router';
import { GeneralTabSettings } from '../../shared/config/generalTabSettings';
import { DemandecinService } from '../../service/demande-cin.service';
import { ROLE, STATUT_DEMANDE } from '../../constants/constant';
import { AccountService } from '../../service/auth/account.service';
import { StorageService } from '../../service/storage/storage.service';

@Component({
  selector: 'ngx-DemandeCin',
  templateUrl: './demande-cin.component.html',
  styleUrls: ['./demande-cin.component.scss']
})
export class DemandeCinComponent implements OnInit {

  readonly perPage = 5

  cols = {
    id: {
      title: 'n°',
      type: 'string',

    },
    nom: {
      title: 'Nom',
      type: 'string',
    },
    prenom: {
      title: 'Prénom',
      type: 'string',
    },
    createdat: {
      title: 'Date de création',
      type: 'string',
      valuePrepareFunction: (data) => {
        return this.datePipe.transform(data, 'dd/MM/yyyy');
      },
    },
    statut: {
      title: 'Statut',
      type: 'string',
      valuePrepareFunction: (data) => {
        if (data) {
          return STATUT_DEMANDE[data].label
        } else {
          return STATUT_DEMANDE['REFUSEE'].label
        }
      },
    }
  };
  settingsCancelled = {
    ...GeneralTabSettings.appearance,
    columns: { ...this.cols },
    actions: false
  };
  settings = {
    ...GeneralTabSettings.appearance,
    columns: { ...this.cols }
  }

  settingsUser = {
    ...GeneralTabSettings.appearance,
    columns: { ...this.cols },
    actions: {
      edit: false,
      delete: false
    }
  };

  source: LocalDataSource = new LocalDataSource();
  sourceCancelled: LocalDataSource = new LocalDataSource();

  readonly Role = ROLE

  params = {
    page: 0,
    size: 5,
    statut: STATUT_DEMANDE.EN_COURS.value
  }
  totalRows = 0
  currentPage = 0

  paramsCancelled = {
    page: 0,
    size: 5,
    statut: STATUT_DEMANDE.REFUSEE.value
  }

  totalRowsCancelled = 0
  currentPageCancelled = 0

  paramsUser = {
    page: 0,
    size: 5,
    idUtilisateur: null
  }

  totalRowsUser = 0
  currentPageUser = 0

  isLoading = false
  isLoadingCancelled = false
  isLoadingUser = false

  keywordSearch = ''
  keywordSearchCancelled = ''

  constructor(
    private accountService: AccountService,
    private service: DemandecinService,
    private storageService: StorageService,
    private router: Router,
    private datePipe: DatePipe,
    private cdrf: ChangeDetectorRef
  ) {

  }

  ngOnInit(): void {

    const user = this.storageService.getUserInfo()
    this.paramsUser.idUtilisateur = user.id

    if (this.isAdmin) {
      this.fetchDataCancelled()
    }
    this.fetchData()
  }

  get isAdmin() {
    return this.accountService.hasRole(this.Role.ROLE_ADMIN.value) || this.accountService.hasRole(this.Role.ROLE_EMPLOYEE.value)
  }

  search(isCancelled = false) {
    if (isCancelled) this.fetchDataCancelled()
    else this.fetchData()
  }

  resetSearch(isCancelled = false) {
    if (isCancelled) {
      this.keywordSearchCancelled = ''
      this.fetchDataCancelled()
    }
    else {
      this.keywordSearch = ''
      this.fetchData()
    }
  }

  fetchData(): any {

    let parameters: any
    if (this.isAdmin) {
      this.isLoading = true
      parameters = this.params
    } else {
      this.isLoadingUser = true
      parameters = this.paramsUser
    }

    if (this.keywordSearch) {
      parameters.keyWord = this.keywordSearch
    } else {
      delete parameters.keyWord
    }


    const subscription = this.isAdmin ? this.service.demandecins(parameters) : this.service.demandecinsUtilisateur(parameters)

    subscription.subscribe(data => {
      this.handleSuccessFetchData(data, false)
    }, error => {
      console.log('Error', error)
      this.handleErrorFetchData(false)
    })
  }

  fetchDataCancelled(): any {

    let parameters: any
    if (this.isAdmin) {
      this.isLoadingCancelled = true
      parameters = this.paramsCancelled
    }
    this.cdrf.detectChanges()

    if (this.keywordSearchCancelled) {
      parameters.keyWord = this.keywordSearchCancelled
    } else {
      delete parameters.keyWord
    }

    const subscription = this.service.demandecins(parameters)

    subscription.subscribe(data => {
      this.handleSuccessFetchData(data, true)
    }, error => {
      console.log('Error', error)
      this.handleErrorFetchData(true)
    })
  }

  handleSuccessFetchData(data, isCancelled) {
    if (this.isAdmin) {

      if (isCancelled) {
        this.setPaginationData(data, true)
        this.sourceCancelled.load(data.content);
        this.isLoadingCancelled = false
      } else {
        this.setPaginationData(data)
        this.source.load(data.content);
        this.isLoading = false
      }
    } else {

      this.setPaginationData(data)
      this.source.load(data.content);
      this.isLoadingUser = false
    }

  }

  handleErrorFetchData(isCancelled) {
    if (isCancelled) {
      this.isLoadingCancelled = false
    } else {
      this.isLoading = false
    }
  }

  setPaginationData(conf: any, isCancelled = false) {
    if (conf) {
      if (!isCancelled) {
        if (this.isAdmin) {
          this.totalRows = conf.totalElements
          this.currentPage = conf.number
        } else {
          this.totalRowsUser = conf.totalElements
          this.currentPageUser = conf.number
        }
      } else {
        this.totalRowsCancelled = conf.totalElements
        this.currentPageCancelled = conf.number
      }

    }

  }

  onDeleteConfirm(event): void {
    console.log('event', event)
    this.isLoading = true
    if (window.confirm('Êtes-vous sûrs de réaliser cette action?')) {
      this.service.delete(event.data.id).subscribe(data => {
        this.fetchData()
      }, error => {
        this.isLoading = false
        console.log('error', error)
      })
    }
  }

  edit(row) {
    console.log('row', row)
    this.router.navigate(['/pages/cin/edit', { id: row.data.id }])
  }

  create(row) {
    console.log('row', row)
    this.router.navigate(['pages/cin/edit'])
  }


  onRowSelect(row) {
    console.log('row', row)
    this.router.navigate(['/pages/demande-cin/detail', { id: row.data.id }])
  }

  onPageChange(event, isCancelled = false): void {
    if (!isCancelled) {
      if (this.isAdmin) this.params.page = event
      else this.paramsUser.page = event

    }
    else this.paramsCancelled.page = event
    this.fetchDataCancelled()
  }

  hasRole(role: string) {
    return this.accountService.hasRole(role)
  }

}
