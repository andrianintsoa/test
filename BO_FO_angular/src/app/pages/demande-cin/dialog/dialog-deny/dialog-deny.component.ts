import { Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ngx-dialog-deny',
  templateUrl: './dialog-deny.component.html',
})
export class DialogDenyComponent {

  formGroup: FormGroup;

  constructor(private fb: FormBuilder, private dialogRef: NbDialogRef<DialogDenyComponent>) {

    this.formGroup = this.fb.group({
      motif: ['', Validators.required],
    });

  }

  closeDialog() {
    this.dialogRef.close(this.formGroup.get('motif').value);
  }

  dismissDialog() {
    this.dialogRef.close();
  }
}


