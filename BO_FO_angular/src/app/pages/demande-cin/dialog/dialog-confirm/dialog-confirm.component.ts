import { Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
})
export class DialogConfirmComponent {

  constructor(private dialogRef: NbDialogRef<DialogConfirmComponent>) { }

  confirm() {
    this.dialogRef.close(true);
  }

  dismissDialog() {
    this.dialogRef.close(false);
  }

}
