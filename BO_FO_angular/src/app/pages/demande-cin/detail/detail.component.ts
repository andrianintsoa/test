import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DemandecinService } from '../../../service/demande-cin.service';
import { environment } from '../../../environments/environment';
import { DatePipe } from '@angular/common';
import { ROLE, STATUT_DEMANDE } from '../../../constants/constant';
import { AccountService } from '../../../service/auth/account.service';
import { NbDialogService } from '@nebular/theme';
import { DialogConfirmComponent } from '../dialog/dialog-confirm/dialog-confirm.component';
import { DialogDenyComponent } from '../dialog/dialog-deny/dialog-deny.component';

@Component({
  selector: 'ngx-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  pdfSrc!: string
  id!: string
  detail: any = {}
  isLoading = false
  baseUrl = environment.apiBaseUrl
  readonly statut = STATUT_DEMANDE
  readonly Role = ROLE

  constructor(
    private accountService: AccountService,
    private route: ActivatedRoute,
    private demandeService: DemandecinService,
    private ref: ChangeDetectorRef,
    private datePipe: DatePipe,
    private dialogService: NbDialogService
  ) {

  }

  ngOnInit(): void {
    this.isLoading = true
    this.id = this.route.snapshot.params['id'];

    this.demandeService.detailDemandeCin(this.id).subscribe(val => {
      this.detail = val
      console.log('motifRefus', val.motifRefus)
      if (this.detail.createdat) {
        this.detail.createdat = this.datePipe.transform(new Date(this.detail.createdat), 'dd/MM/yyyy')
      }
      if (this.detail.datenaissance) {
        this.detail.datenaissance = this.datePipe.transform(new Date(this.detail.datenaissance), 'dd/MM/yyyy')
      }
      if (this.detail.photoUrl) {
        this.detail.photoUrl = environment.apiBaseUrl + this.detail.photoUrl
      }

      if (!this.detail.statut) {
        this.detail.statut = this.statut.EN_COURS.value
      }

      this.detail.typeDocumentDemandeCins = val.typeDocumentDemandeCins.map(elt => {
        return { ...elt, pathUrl: environment.apiBaseUrl + elt.pathUrl }
      })


      this.ref.detectChanges()
      console.log('detail', this.detail)
      this.isLoading = false
    }, error => {
      this.isLoading = false
      console.log('error', error)
    })
  }

  changeStatus(statutApply: string) {

    if (statutApply === this.statut.ACCEPTEE.value) {

      this.openConfirmationDialog().onClose.subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.isLoading = true;
          // Appel à l'API pour mettre à jour le statut
          this.demandeService.updateStatutDemandecin(this.detail.id, statutApply).subscribe(
            val => {
              this.isLoading = false;
              this.detail.statut = statutApply;
            },
            error => {
              this.isLoading = false;
              console.log('error', error);
            }
          );
        }
      });
    } else if (statutApply === this.statut.REFUSEE.value) {

      this.openMotifDialog().onClose.subscribe((motif: string) => {
        if (motif) {
          this.isLoading = true;
          // Appel à l'API pour mettre à jour le statut avec le motif
          this.demandeService.updateStatutDemandecin(this.detail.id, statutApply, motif).subscribe(
            val => {
              this.isLoading = false;
              this.detail.statut = statutApply;
            },
            error => {
              this.isLoading = false;
              console.log('error', error);
            }
          );
        }
      });
    }
  }

  openConfirmationDialog() {
    return this.dialogService.open(DialogConfirmComponent, {
      hasBackdrop: true,
      closeOnBackdropClick: false,
      closeOnEsc: false,
    });
  }

  openMotifDialog() {
    return this.dialogService.open(DialogDenyComponent, {
      hasBackdrop: true,
      closeOnBackdropClick: false,
      closeOnEsc: false,
    });
  }


  hasRole(role: string) {
    return this.accountService.hasRole(role)
  }
}
