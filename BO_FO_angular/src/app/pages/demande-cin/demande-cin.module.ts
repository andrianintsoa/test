import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbStepperModule, NbButtonModule, NbTabsetModule, NbAccordionModule, NbSpinnerModule, NbBadgeModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PdfViewerModule } from 'ng2-pdf-viewer';


import { DemandeCinRoutingModule } from './demande-cin-routing.module';
import { DemandeCinComponent } from './demande-cin.component';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { DetailComponent } from './detail/detail.component';

import { SharedModule } from '../../shared/shared.module';
import { CinModule } from '../cin/cin.module';
import { DialogConfirmComponent } from './dialog/dialog-confirm/dialog-confirm.component';
import { DialogDenyComponent } from './dialog/dialog-deny/dialog-deny.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DemandeCinComponent,
    CreateUpdateComponent,
    DetailComponent,
    DialogConfirmComponent,
    DialogDenyComponent
  ],
  imports: [
    CommonModule,
    DemandeCinRoutingModule,
    NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbStepperModule, NbButtonModule, NbTabsetModule, NbAccordionModule, NbSpinnerModule, NbBadgeModule,
    Ng2SmartTableModule, PdfViewerModule, NbTreeGridModule,
    Ng2SmartTableModule,
    SharedModule,
    CinModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class DemandeCinModule { }
