import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemandeCinComponent } from './demande-cin.component';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: DemandeCinComponent,
    },
    {
      path: 'edit',
      component: CreateUpdateComponent,
    },
    {
      path: 'edit/:id',
      component: CreateUpdateComponent,
    },
    {
      path: 'detail',
      component: DetailComponent,
    },
    {
      path: 'detail/:id',
      component: DetailComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemandeCinRoutingModule { }
