import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateUpdateComponent } from './create-update/create-update.component';
import { CinComponent } from './cin.component';
import { DetailComponent } from './detail/detail.component';
import { RoleGuardService } from '../../guards/role-guard.service';
import { ROLE } from '../../constants/constant';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: CinComponent,
    },
    {
      path: 'edit',
      component: CreateUpdateComponent,
      canActivate: [RoleGuardService],
      data: { roles: [ROLE.ROLE_ADMIN.value] },
    },
    {
      path: 'edit/:id',
      component: CreateUpdateComponent,
      canActivate: [RoleGuardService],
      data: { roles: [ROLE.ROLE_ADMIN.value] },
    },
    {
      path: 'detail/:id',
      component: DetailComponent,
    },
    {
      path: 'detail',
      component: DetailComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CinRoutingModule { }
