import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { DatePipe } from '@angular/common';

import { MOTIF_DEMANDE, SEXE, STATUT_DEMANDE } from '../../../constants/constant';
import { StorageService } from '../../../service/storage/storage.service';
import { TypeDocumentService } from '../../../service/type-document.service';
import { DemandecinService } from '../../../service/demande-cin.service';
import { CommuneService } from '../../../service/commune.service';


@Component({
  selector: 'ngx-cin-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent implements OnInit {

  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  uploadForm: FormGroup;

  typeDocsList = [];
  selectedFilesToUpload = {}
  selectedPhoto = {}
  isLoading = false;
  isInitialized = false;

  readonly SEXE = SEXE

  communeOptions: any[];

  motifOptions = Object.values(MOTIF_DEMANDE);
  readonly Motif = MOTIF_DEMANDE;

  title = ''

  isEditonMode = false

  picturePreview: string | ArrayBuffer = ''

  constructor(
    private fb: FormBuilder,
    private typeDocumentService: TypeDocumentService,
    private demandecinService: DemandecinService,
    private storageService: StorageService,
    private communeService: CommuneService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
  ) {
  }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');

    if (id) this.isEditonMode = true

    this.title = id ? 'Modification demande' : 'Création d\'une demande de cin'

    this.firstForm = this.fb.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      dateNaissance: ['', [Validators.required, this.validateAge]],
      lieuNaissance: ['', Validators.required],
      sexe: ['', Validators.required],
      photo: ['', this.isEditonMode ? [] : Validators.required],
    });

    this.secondForm = this.fb.group({
      nomPere: ['', Validators.required],
      nomMere: ['', Validators.required],
      adresse: ['', Validators.required],
      profession: ['', Validators.required],
      commune: [, Validators.required],
    });

    this.isLoading = true

    forkJoin(this.typeDocumentService.typedocuments(), this.communeService.communes()).subscribe(
      ([response1, response2]) => {

        this.typeDocsList = response1

        const formControls = {
          motif: ['', Validators.required],
          numerocin: ['', Validators.pattern('[0-9]*')],
        };

        if (!this.isEditonMode) { // only on creation
          this.typeDocsList.forEach(doc => {
            // Ajouter chaque champ au formulaire avec une validation requise
            formControls[doc.nom] = [null, Validators.required];
            this.selectedFilesToUpload[doc.nom] = null
          });
        }
        // Parcourir la liste typeDocsList


        // Créer le FormGroup à partir des champs du formulaire
        this.uploadForm = this.fb.group(formControls);
        this.isInitialized = true

        this.communeOptions = response2

        if (!this.isEditonMode) this.isLoading = false

        this.uploadForm.get('motif')?.valueChanges.subscribe((motifValue) => {
          const numerocinControl = this.uploadForm.get('numerocin');
          if (motifValue !== MOTIF_DEMANDE.CREATION.value) {
            numerocinControl?.setValidators([Validators.required, Validators.pattern('[0-9]*')]);
          } else {
            numerocinControl?.setValidators([Validators.pattern('[0-9]*')]);
          }
          numerocinControl.setValue('')
          numerocinControl?.updateValueAndValidity();
        });

        if (this.isEditonMode) {
          this.demandecinService.detailDemandeCin(id).subscribe(
            (val) => {
              this.firstForm.patchValue({
                nom: val.nom,
                prenom: val.prenom,
                dateNaissance: this.datePipe.transform(new Date(val.datenaissance), 'yyyy-MM-dd'),
                lieuNaissance: val.lieunaissance,
                sexe: val.sexe,
                photo: val.photoUrl,
              })
              this.secondForm.patchValue({
                nomPere: val.nompere,
                nomMere: val.nommere,
                adresse: val.adresse,
                profession: val.profession,
                commune: val.commune.id
              })

              this.uploadForm.patchValue(
                {
                  motif: val.motif,
                  numerocin: val.numerocin
                }
              )
              this.isLoading = false
            },
            error => {
              this.isLoading = false
              console.log('error', error)
            }
          )
        } else {
          this.isLoading = false
        }

      }
    )
  }

  validateAge(control: AbstractControl): { [key: string]: boolean } | null {
    const birthDate = new Date(control.value);
    const currentDate = new Date();
    const ageDiff = currentDate.getFullYear() - birthDate.getFullYear();

    if (ageDiff < 18) {
      return { 'ageInvalid': true };
    }
    return null;
  }

  onFirstSubmit() {
    this.firstForm.markAsDirty();
  }

  onSecondSubmit() {
    this.secondForm.markAsDirty();
  }

  onThirdSubmit() {
    console.log('onThirdSubmit')
    this.uploadForm.markAsDirty();
    this.mainSubmit();
  }

  onFileSelected(event: any, fieldName: string) {
    const control = this.uploadForm.get(fieldName);

    if (control instanceof FormControl) {

      let file = event.target.files[0]
      let mimeType = file.type;

      if (mimeType !== 'application/pdf') {
        this.uploadForm.get(fieldName).setValue(null)
        this.selectedFilesToUpload[fieldName] = null;
      } else {
        this.selectedFilesToUpload[fieldName] = file;
      }
    } else {
      this.selectedFilesToUpload[fieldName] = null;
    }
  }

  onPhotoSelected(event: any,) {
    if (event.target.files.length) {

      let file = event.target.files[0]
      let mimeType = file.type;

      if (mimeType.match(/image\/*/) == null) {
        this.firstForm.get('photo').setValue(null)
        this.selectedPhoto = null;
        this.picturePreview = null
      } else {
        this.selectedPhoto = event.target.files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = (_event) => {
          this.picturePreview = reader.result;
        }
      }
    }

    else {
      this.selectedPhoto = null;
      this.picturePreview = null
    }

  }

  mainSubmit() {
    this.isLoading = true
    const userConnected = this.storageService.getUserInfo()
    const payload = {
      demande: {
        numerocin: this.uploadForm.get('numerocin')?.value,
        sexe: this.firstForm.get('sexe')?.value,
        nom: this.firstForm.get('nom')?.value,
        prenom: this.firstForm.get('prenom')?.value,
        lieunaissance: this.firstForm.get('lieuNaissance')?.value,
        datenaissance: this.firstForm.get('dateNaissance')?.value,
        nompere: this.secondForm.get('nomPere')?.value,
        nommere: this.secondForm.get('nomMere')?.value,
        adresse: this.secondForm.get('adresse')?.value,
        profession: this.secondForm.get('profession')?.value,
        statut: STATUT_DEMANDE.EN_COURS.value,
        utilisateur: { ...userConnected },
        commune: this.communeOptions.find(c => c.id == this.secondForm.get('commune')?.value),
        motifDemande: this.uploadForm.get('motif')?.value
      },
      docs: [],
      photo: this.selectedPhoto
    }

    if (!this.isEditonMode) {
      for (let property in this.selectedFilesToUpload) {
        payload.docs.push(this.selectedFilesToUpload[property]);
      }
    }
    let subscription
    if (this.isEditonMode) {
      subscription = this.demandecinService.editDemandecin({ ...payload.demande, id: this.route.snapshot.paramMap.get('id') })
    } else {
      subscription = this.demandecinService.createDemandecinWithDocuments(payload)
    }
    subscription.subscribe(
      data => {
        console.log('data', data)
        this.isLoading = false
        this.back()
      }, error => {
        this.isLoading = false
      }
    )

  }

  back(): void {
    window.history.back();
  }

}
