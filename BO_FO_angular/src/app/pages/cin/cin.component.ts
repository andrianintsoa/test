import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';

import { GeneralTabSettings } from '../../shared/config/generalTabSettings';
import { CinService } from '../../service/cin.service';
import { escapeRegExp } from '../../shared/helpers/utils';
import { DatePipe } from '@angular/common';
import { AccountService } from '../../service/auth/account.service';
import { StorageService } from '../../service/storage/storage.service';
import { ROLE } from '../../constants/constant';
@Component({
  selector: 'ngx-cin',
  templateUrl: './cin.component.html',
  styleUrls: ['./cin.component.scss']
})
export class CinComponent implements OnInit {

  settings = {
    ...GeneralTabSettings.appearance,
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      numero: {
        title: 'N°',
        type: 'number',
      },
      nom: {
        title: 'Nom',
        type: 'string',
      },
      prenom: {
        title: 'Prénom',
        type: 'string',
      },
      adresse: {
        title: 'Adresse',
        type: 'string',
      },
      createdat: {
        title: 'Date de création',
        type: 'string',
      },
    },
    actions: false
  };

  source: LocalDataSource = new LocalDataSource();

  isLoading = false;

  constructor(
    private router: Router,
    private service: CinService,
    private accountService: AccountService,
    private storageService: StorageService
  ) {
    this.source.load([]);
  }

  ngOnInit() {
    this.getData()
  }

  getData() {
    this.isLoading = true
    const user = this.storageService.getUserInfo();

    let subscription = this.accountService.hasRole(ROLE.ROLE_USER.value) ? this.service.cinsByUser(user.id) : this.service.cins()
    subscription.subscribe(data => {
      const formattedData = data.map(elt => {
        return {

          numero: elt.numero,
          ...elt.demandecin,
          id: elt.id,
          createdat: new DatePipe('fr').transform(new Date(elt.createdat), 'dd/MM/yyyy')
        }
      }, error => {
        this.isLoading = false
        console.log('error', error)
      })
      console.log('data', data)
      this.source.load(formattedData);
      this.isLoading = false
    })
  }

  onDeleteConfirm(event): void {
    this.isLoading = true
    if (window.confirm('Êtes-vous sûrs de réaliser cette action ?')) {
      this.service.delete(event.data.id).subscribe(data => {
        this.getData()
      }, error => {
        this.isLoading = false
        console.log('error', error)
      })
    }
  }

  delete(event) {
    console.log('event delete', event)
  }

  edit(row) {
    console.log('row', row)
    this.router.navigate(['pages/cin/edit', { id: row.data.id }])
  }

  create(row) {
    console.log('row', row)
    this.router.navigate(['pages/cin/edit'])
  }


  onRowSelect(row) {
    console.log('row', row)
    this.router.navigate(['pages/cin/detail', { id: row.data.id }])
  }


}
