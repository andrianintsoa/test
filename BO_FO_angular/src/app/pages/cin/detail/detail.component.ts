import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { DatePipe } from '@angular/common';
import { CinService } from '../../../service/cin.service';

@Component({
  selector: 'ngx-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  pdfSrc!: string
  id!: string
  detail: any = {}
  isLoading = false
  baseUrl = environment.apiBaseUrl

  constructor(
    private route: ActivatedRoute,
    private service: CinService,
    private ref: ChangeDetectorRef,
    private datePipe: DatePipe
  ) {

  }

  ngOnInit(): void {
    this.isLoading = true
    this.id = this.route.snapshot.params['id'];

    this.service.detailCin(this.id).subscribe(val => {
      this.detail = {
        id: val.id,
        numero: val.numero,
        duplicata: val.duplicata,
        expired: val.expired,
        pathqrcode: environment.apiBaseUrl + val.pathqrcode,
        ...val.demandecin,
        photoUrl: environment.apiBaseUrl + val.demandecin.photoUrl,
        datenaissance: this.datePipe.transform(new Date(val.demandecin.datenaissance), 'dd/MM/yyyy'),
        createdat: new DatePipe('fr').transform(new Date(val.createdat), 'dd/MM/yyyy'),
        typeDocumentDemandeCins: val.demandecin.typeDocumentDemandeCins.map(elt => {
          return { ...elt, pathUrl: environment.apiBaseUrl + elt.pathUrl }
        })
      }

      this.ref.detectChanges()
      console.log('detail', this.detail)
      this.isLoading = false
    }, error => {
      this.isLoading = false
      console.log('error', error)
    })
  }
}
