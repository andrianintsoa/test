import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbStepperModule, NbButtonModule, NbTabsetModule, NbAccordionModule, NbRadioModule, NbSelectModule, NbSpinnerModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PdfViewerModule } from 'ng2-pdf-viewer';


import { CinRoutingModule } from './cin-routing.module';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { CinComponent } from './cin.component';

import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailComponent } from './detail/detail.component';


@NgModule({
  declarations: [
    CreateUpdateComponent,
    CinComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    CinRoutingModule,
    NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbStepperModule, NbButtonModule, NbTabsetModule, NbAccordionModule, NbSpinnerModule, NbRadioModule, NbSelectModule,
    Ng2SmartTableModule, PdfViewerModule,
    SharedModule
  ],
  exports: [
    CreateUpdateComponent
  ]
})
export class CinModule { }
