import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { Router } from '@angular/router';
import { GeneralTabSettings } from '../../shared/config/generalTabSettings';
import { TypeDocumentService } from '../../service/type-document.service';

@Component({
  selector: 'ngx-TypeDocument',
  templateUrl: './type-document.component.html',
  styleUrls: ['./type-document.component.scss']
})
export class TypeDocumentComponent {

  settings = {
    ...GeneralTabSettings.appearance,
    columns: {
      id: {
        title: 'n°',
        type: 'string',

      },
      nom: {
        title: 'Nom',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  isLoading = false

  constructor(
    private service: TypeDocumentService,
    private router: Router,
  ) {
    this.fetchData()
  }

  fetchData(): any {
    this.isLoading = true
    this.service.typedocuments().subscribe(data => {
      console.log('data', data)
      const dataSource = data
      this.source.load(dataSource);
      this.isLoading = false
    }, error => {
      this.isLoading = false
      console.log('error', error)
    })
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Êtes-vous sûrs de réaliser cette action?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  edit(row) {
    console.log('row', row)
    this.router.navigate(['/pages/type-document/edit', { id: row.data.id }])
  }

  create(row) {
    console.log('row', row)
    this.router.navigate(['pages/type-document/edit'])
  }


}
