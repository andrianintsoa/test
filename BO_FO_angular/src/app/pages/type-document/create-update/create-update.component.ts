import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TypeDocumentService } from '../../../service/type-document.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-cin-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.scss']
})
export class CreateUpdateComponent implements OnInit {

  formGroup: FormGroup;

  dataToEdit?: any;
  isLoading = false

  title = ''

  constructor(
    private fb: FormBuilder,
    private service: TypeDocumentService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this.title = id ? 'Modification' : 'Création'
    this.formGroup = this.fb.group({
      nom: ['', [Validators.required, Validators.pattern(/^[a-zA-Z\s]*$/)]],
    });

    if (id) this.initForm(id)
  }


  initForm(id): void {
    this.isLoading = true
    this.service.detailTypeDocument(id).subscribe(data => {
      this.dataToEdit = data
      this.formGroup.patchValue({
        nom: this.dataToEdit.nom.replace(/\./g, ''),
      });
      this.isLoading = false
    }, error => {
      this.isLoading = false
      console.log('error', error)
    });

  }


  submit() {
    const payload = {
      nom: this.formGroup.get('nom')?.value.replace(/\./g, ''),
    }
    let subscription = this.dataToEdit?.id ? this.service.editTypeDocument({ id: this.dataToEdit.id, ...payload }) : this.service.addTypeDocument(payload);

    subscription.subscribe(
      data => {
        console.log('data', data)
        this.back()
      }
    )

  }

  back(): void {
    window.history.back();
  }


}
