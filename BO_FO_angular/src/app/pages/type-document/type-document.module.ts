import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule, NbSpinnerModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PdfViewerModule } from 'ng2-pdf-viewer';


import { TypeDocumentRoutingModule } from './type-document-routing.module';
import { CreateUpdateComponent } from './create-update/create-update.component';
import { TypeDocumentComponent } from './type-document.component';

import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CreateUpdateComponent,
    TypeDocumentComponent,
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    TypeDocumentRoutingModule,
    NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule, NbSpinnerModule,
    Ng2SmartTableModule, PdfViewerModule,
    SharedModule
  ]
})
export class TypeDocumentModule { }
