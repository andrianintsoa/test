import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateUpdateComponent } from './create-update/create-update.component';
import { TypeDocumentComponent } from './type-document.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: TypeDocumentComponent,
    },
    {
      path: 'edit',
      component: CreateUpdateComponent,
    },
    {
      path: 'edit/:id',
      component: CreateUpdateComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypeDocumentRoutingModule { }
