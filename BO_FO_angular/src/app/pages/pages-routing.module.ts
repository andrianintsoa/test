import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuardService } from '../guards/auth-guard.service';

import { PagesComponent } from './pages.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { ROLE } from '../constants/constant';
import { RoleGuardService } from '../guards/role-guard.service';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { HomePageComponent } from './home-page/home-page.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  canActivate: [AuthGuardService],
  children: [
    {
      path: 'cin',
      loadChildren: () => import('./cin/cin.module')
        .then(m => m.CinModule),

    },
    {
      path: 'demande-cin',
      loadChildren: () => import('./demande-cin/demande-cin.module')
        .then(m => m.DemandeCinModule),
    },
    {
      path: 'commune',
      loadChildren: () => import('./commune/commune.module')
        .then(m => m.CommuneModule),
      canActivate: [RoleGuardService],
      data: { roles: [ROLE.ROLE_ADMIN.value] },
    },
    {
      path: 'type-document',
      loadChildren: () => import('./type-document/type-document.module')
        .then(m => m.TypeDocumentModule),
      canActivate: [RoleGuardService],
      data: { roles: [ROLE.ROLE_ADMIN.value] },
    },
    {
      path: 'utilisateur',
      loadChildren: () => import('./utilisateur/utilisateur.module')
        .then(m => m.UtilisateurModule),
      canActivate: [RoleGuardService],
      data: { roles: [ROLE.ROLE_ADMIN.value] },
    },
    {
      path: 'historique-scan',
      loadChildren: () => import('./historique-scan/historique-scan.module')
        .then(m => m.HistoriqueScanModule),
      canActivate: [RoleGuardService],
      data: { roles: [ROLE.ROLE_USER.value] },
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: 'unauthorized',
      component: UnauthorizedComponent
    },
    {
      path: '',
      component: HomePageComponent
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
