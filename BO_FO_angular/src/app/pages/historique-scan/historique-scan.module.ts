import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule, NbSpinnerModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PdfViewerModule } from 'ng2-pdf-viewer';



import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HistoriqueScanComponent } from './historique-scan.component';
import { HistoriqueScanRoutingModule } from './historique-scan-routing.module';


@NgModule({
  declarations: [
    HistoriqueScanComponent
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    HistoriqueScanRoutingModule,
    NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbButtonModule, NbSpinnerModule,
    Ng2SmartTableModule, PdfViewerModule,
    SharedModule
  ]
})
export class HistoriqueScanModule { }
