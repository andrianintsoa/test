import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { GeneralTabSettings } from '../../shared/config/generalTabSettings';
import { CinService } from '../../service/cin.service';
import { StorageService } from '../../service/storage/storage.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-historique-scan',
  templateUrl: './historique-scan.component.html',
})
export class HistoriqueScanComponent {


  settings = {
    ...GeneralTabSettings.appearance,
    columns: {
      date: {
        title: 'Date de scan',
        type: 'string',
      },
      nom: {
        title: 'Nom',
        type: 'string',
      },
      prenom: {
        title: 'Prénom',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
    },
    actions: false
  };

  source: LocalDataSource = new LocalDataSource();

  isLoading = false

  params = {
    page: 0,
    size: 5,
    idUtilisateur: 0
  }
  totalRows = 0
  currentPage = 0

  constructor(
    private service: CinService,
    private storageService: StorageService,
    private datePipe: DatePipe,
  ) {
    const user = this.storageService.getUserInfo()
    this.params.idUtilisateur = user.id
    this.fetchData()
  }

  fetchData(): any {
    this.isLoading = true

    this.service.historiqueScan(this.params).subscribe(data => {
      console.log('data', data)
      const dataSource = data.docs.map(elt => {
        return {
          date: this.datePipe.transform(elt.date, 'dd/MM/yyyy'),
          nom: elt.utilisateurScanner.nom ?? '-',
          prenom: elt.utilisateurScanner.prenom ?? '-',
          email: elt.utilisateurScanner.email ?? '-',
        }
      })
      this.source.load(dataSource)
      this.setPaginationData(data)
      this.isLoading = false
    }, error => {
      this.isLoading = false
      console.log('error', error)
    })
  }

  setPaginationData(data) {
    if (data) {

      this.totalRows = data.totalDocs
      this.currentPage = data.page


    }

  }

  onPageChange(event, isCancelled = false): void {
    if (!isCancelled) {
      this.params.page = event + 1

    }

    this.fetchData()
  }

}
