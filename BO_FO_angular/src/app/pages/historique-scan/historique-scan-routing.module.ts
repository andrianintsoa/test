import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HistoriqueScanComponent } from './historique-scan.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: HistoriqueScanComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoriqueScanRoutingModule { }
