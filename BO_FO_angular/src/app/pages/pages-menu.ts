import { NbMenuItem } from '@nebular/theme';

export const MENU_ADMIN_ITEMS: NbMenuItem[] = [

  {
    title: 'Carte d\'identité',
    icon: 'credit-card-outline',
    children: [
      {
        title: 'Liste cin',
        link: '/pages/cin',
      },

      {
        title: 'Demandes cin',
        link: '/pages/demande-cin',
      },

    ],
  },
  {
    title: 'Commune',
    icon: 'pin-outline',
    link: '/pages/commune',
  },
  {
    title: 'Documents cin',
    icon: 'layers-outline',
    link: '/pages/type-document',
  },
  {
    title: 'Utilisateurs',
    icon: 'people-outline',
    link: '/pages/utilisateur',
  },
];

export const MENU_EMPLOYEE_ITEMS: NbMenuItem[] = [

  {
    title: 'Demandes cin',
    link: '/pages/demande-cin',
    icon: 'credit-card-outline',

  }
];

export const MENU_PUBLIC_ITEMS: NbMenuItem[] = [

  {
    title: 'Mes demandes',
    icon: 'credit-card-outline',
    link: '/pages/demande-cin',
  },
  {
    title: 'Mes cartes',
    icon: 'credit-card-outline',
    link: '/pages/cin',
  },
  {
    title: 'Historique de scan',
    icon: 'cube-outline',
    link: '/pages/historique-scan',
  }

];
