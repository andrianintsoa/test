export const environment = {
  production: false,
  // apiBaseUrl: 'https://spring-tptp9.onrender.com',
  // apiMongoBaseUrl: 'https://tpt-api-node-p9.onrender.com',
  apiBaseUrl: 'http://localhost:8080',
  apiMongoBaseUrl: 'http://localhost:8010',
};
