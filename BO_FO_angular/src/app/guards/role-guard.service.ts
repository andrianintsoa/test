import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AccountService } from '../service/auth/account.service';
import { StorageService } from '../service/storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService {

  constructor(private _router: Router, private service: StorageService) {

  }

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {

    const user = this.service.getUserInfo();

    const rolesAble = next.data.roles;

    console.log(rolesAble)

    if (!rolesAble || rolesAble?.includes(user.role.nom)) return true


    this._router.navigate(['pages/unauthorized']);

    return false;
  }
}
