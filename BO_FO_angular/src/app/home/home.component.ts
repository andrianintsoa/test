import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-home',
  templateUrl: './home.component.html',
   styleUrls: ['./home.component.scss'],
  // template: `
  //   <ngx-one-column-layout>
  //     <router-outlet></router-outlet>
  //   </ngx-one-column-layout>
  // `,
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
