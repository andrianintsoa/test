export class GeneralTabSettings {

  static appearance = {
    mode: 'external',
    actions: {
      position: "left",
      columnTitle: "Actions"
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>'
    },
  };

}
