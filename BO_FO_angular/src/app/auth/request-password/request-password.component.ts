import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { NbRequestPasswordComponent, NbAuthService, NB_AUTH_OPTIONS } from '@nebular/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-request-password',
  templateUrl: './request-password.component.html',
  styleUrls: ['./request-password.component.scss']
})
export class RequestPasswordComponent extends NbRequestPasswordComponent implements OnInit {

  constructor(protected service: NbAuthService, @Inject(NB_AUTH_OPTIONS) protected options = {}, protected cd: ChangeDetectorRef, protected router: Router) {
    super(service, options, cd, router);

    // if(this.account.isLoggedIn){
    //   	this.router.navigate(['app/accueil']);
    // }

  }

  requestPass(): void {
    this.errors = [];
    this.messages = [];


  }

  ngOnInit() {
  }

}
