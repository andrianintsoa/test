import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { NbResetPasswordComponent, NbAuthService, NB_AUTH_OPTIONS } from '@nebular/auth';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent extends NbResetPasswordComponent implements OnInit {

  actionCode;

  constructor(protected service: NbAuthService, @Inject(NB_AUTH_OPTIONS) protected options = {}, protected cd: ChangeDetectorRef, protected router: Router, private activatedRoute: ActivatedRoute) {
    super(service, options, cd, router);

    // if(this.account.isLoggedIn){
    //   	this.router.navigate(['app/accueil']);
    // }

  }

  resetPass(): void {
    this.errors = [];
    this.messages = [];
    // this.submitted = true;



  }

  ngOnInit() {
    this.activatedRoute.queryParams
      .subscribe(params => {

        if (!params) this.router.navigate(['app/accueil']);

        this.actionCode = params['oobCode'];

        // try{
        // 	this.account.auth.verifyPasswordResetCode(this.actionCode).then(email => {

        // 	}).catch(e => {
        // 		//console.log('eroro dsqd');
        // 		this.router.navigate(['auth/login']);
        // 	});
        // } catch(e){
        // 	this.router.navigate(['auth/login']);
        // }

      })
  }

}
