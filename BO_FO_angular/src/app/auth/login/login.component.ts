import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { NbLoginComponent, NbAuthService, NB_AUTH_OPTIONS } from '@nebular/auth';
import { Router } from '@angular/router';
import { AccountService } from '../../service/auth/account.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends NbLoginComponent implements OnInit {

  getDocSub;

  constructor(
    protected service: NbAuthService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected cd: ChangeDetectorRef,
    protected router: Router,
    private accountService: AccountService
  ) {
    super(service, options, cd, router);

    // if(this.account.isLoggedIn){
    //   	this.router.navigate(['app/accueil']);
    // }

  }

  login() {
    this.errors = [];
    this.submitted = true;

    console.log(this.user.email, this.user.password)

    this.accountService.login({ email: this.user.email, password: this.user.password }).subscribe(res => {
      if (res) {
        this.router.navigate(['/pages'])
      }
    }, (error) => {

      this.submitted = false;

      var errorCode = error.code;
      var errorMessage = error.message;


      this.errors.push(errorMessage);

      this.cd.detectChanges();

    })
  }

  ngOnInit() {
  }

}
