import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { NbRegisterComponent, NbAuthService, NB_AUTH_OPTIONS } from '@nebular/auth';
import { Router } from '@angular/router';
import { AccountService } from '../../service/auth/account.service';

@Component({
  selector: 'ngx-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent extends NbRegisterComponent implements OnInit {

  loading = false;
  user: any = {}

  constructor(
    protected service: NbAuthService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected cd: ChangeDetectorRef,
    protected router: Router,
    private accountService: AccountService
  ) {
    super(service, options, cd, router);
  }

  register(): void {
    this.errors = [];
    this.submitted = true;
    const payload = {
      ...this.user
    }
    delete payload.confirmPassword


    this.accountService.signUp(payload).subscribe(res => {
      if (res) {
        this.router.navigate(['/'])
      }
    }, (error) => {

      this.submitted = false;

      var errorCode = error.code;
      var errorMessage = error.message;


      this.errors.push(errorMessage);

      this.cd.detectChanges();

    })



  }

  ngOnInit() {

  }


}
