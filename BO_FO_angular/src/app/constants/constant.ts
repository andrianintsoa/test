export const STATUT_DEMANDE = {
  EN_COURS: { label: 'En cours', value: 'EN_COURS' },
  ACCEPTEE: { label: 'Acceptée', value: 'ACCEPTEE' },
  REFUSEE: { label: 'Refusée', value: 'REFUSEE' }
}


export const SEXE = {
  HOMME: { label: 'Homme', value: 'HOMME' },
  FEMME: { label: 'Femme', value: 'FEMME' }
}

export const ROLE = {
  ROLE_ADMIN: { label: 'Administrateur', value: 'ROLE_ADMIN' },
  ROLE_EMPLOYEE: { label: 'Employé', value: 'ROLE_EMPLOYEE' },
  ROLE_USER: { label: 'Simple utilisateur', value: 'ROLE_USER' }
}

export const MOTIF_DEMANDE = {
  PERTE: { label: 'Perte', value: 'PERTE' },
  RENOUVELLEMENT: { label: 'Renouvellement', value: 'RENOUVELLEMENT' },
  CREATION: { label: 'Création', value: 'CREATION' },
}
